package jp.itnav.mindfulness;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

public abstract class BaseToolBarActivity extends BaseActivity {
    protected abstract int getToolBarId();
    protected abstract int getToolBarMenu();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void setToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(getToolBarId());
        toolbar.inflateMenu(getToolBarMenu());
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                startActivity(TutorialActivity.class);
                return true;
            }
        });
    }
}

package jp.itnav.mindfulness;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;

import jp.itnav.mindfulness.Data.ScenesData;
import jp.itnav.mindfulness.Data.SoundsData;
import jp.itnav.mindfulness.dialog.SelectSceneDialog;
import jp.itnav.mindfulness.dialog.SelectSoundDialog;

public class SelectSceneActivity extends BaseToolBarActivity implements View.OnClickListener {
    private ImageButton soundSelectButton;
    private ImageButton sceneSelectButton;
    private ImageButton startButton;
    private int[] selectedSoundsList;
    private int sceneType = 0;

    @Override
    protected int getToolBarId() {
        return R.id.tool_bar;
    }

    @Override
    protected int getToolBarMenu() {
        return R.menu.menu_tutorial;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_scene);
        setLayout();
        selectedSoundsList = SoundsData.getDefaultSound();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            // 音選択
            case R.id.button_select_sound:
                SelectSoundDialog soundDialog = new SelectSoundDialog() {
                    @Override
                    public void onDismissSoundDialog(int[] selectedSounds, int imageId) {
                        soundSelectButton.setImageResource(imageId);
                        selectedSoundsList = selectedSounds;
                    }
                };
                soundDialog.show(getFragmentManager(), "", new SoundsData());
                break;
            // シーン(時間)選択
            case R.id.button_select_scene:
                SelectSceneDialog dialog = new SelectSceneDialog() {
                    @Override
                    public void onDismissSceneDialog(ScenesData.SceneData data, int position) {
                        if (data != null) {
                            sceneSelectButton.setImageResource(data.getDisplayImageId());
                            sceneType = position;
                        }
                    }
                };
                dialog.show(getFragmentManager(), "", new ScenesData());
                break;
            case R.id.button_meditation_start:
                startActivity(MeditationActivity.class, "sound", selectedSoundsList[sceneType]);
                break;
        }
    }

    @Override
     public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    protected void setLayout() {
        setToolBar();
        soundSelectButton = (ImageButton) findViewById(R.id.button_select_sound);
        sceneSelectButton = (ImageButton) findViewById(R.id.button_select_scene);
        startButton = (ImageButton) findViewById(R.id.button_meditation_start);
        soundSelectButton.setOnClickListener(this);
        sceneSelectButton.setOnClickListener(this);
        startButton.setOnClickListener(this);
    }
}

package jp.itnav.mindfulness;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import jp.itnav.mindfulness.dialog.TopMenuDialog;

public class MainActivity extends BaseToolBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setLayouts();
    }
    @Override
    protected int getToolBarId() {
        return R.id.tool_bar;
    }

    @Override
    protected int getToolBarMenu() {
        return R.menu.menu_tutorial;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.button_top_start:
                if (isFinishedTutorial()) {
                    startActivity(SelectSceneActivity.class);
                }
                else {
                    startActivity(TutorialActivity.class);
                }
                break;
            case R.id.button_top_about:
                showDescriptionDialog();
                break;
        }
    }

    private void setLayouts() {
        setToolBar();
        ImageButton buttonStart = (ImageButton) findViewById(R.id.button_top_start);
        buttonStart.setOnClickListener(this);
        ImageButton buttonAbout = (ImageButton) findViewById(R.id.button_top_about);
        buttonAbout.setOnClickListener(this);
    }

    private void showDescriptionDialog() {
        TopMenuDialog dialog = new TopMenuDialog();
        dialog.show(getFragmentManager(),"");
    }
}

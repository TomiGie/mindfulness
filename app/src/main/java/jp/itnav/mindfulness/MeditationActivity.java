package jp.itnav.mindfulness;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import jp.itnav.mindfulness.Data.SoundsData;

public class MeditationActivity extends BaseActivity {
    private MediaPlayer mediaPlayer;
    private AudioManager audioManager;
    private ImageButton soundButton;
    private SeekBar volumeSeekBar;
    private int soundResource;
    private TickHandler tickHandler;
    private ProgressBar soundProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meditation);
        soundResource = getIntent().getIntExtra("sound", SoundsData.getDefaultSound()[0]);
    }

    @Override
    protected void onStop() {
        super.onStop();
        releaseMediaPlayer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setMedia();
        setLayout();
        playSound();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sound_button:
                soundAction();
                break;
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        boolean key = super.onKeyUp(keyCode, event);
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN
                || keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            updateSeekBarSoundState();
        }
        return key;
    }

    @Override
    public void onBackPressed() {
        releaseMediaPlayer();
        super.onBackPressed();
    }

    private void setLayout() {
        soundButton = (ImageButton) findViewById(R.id.sound_button);
        soundButton.setOnClickListener(this);

        int currentVolume = getCurrentVolume();
        int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        soundProgressBar = (ProgressBar) findViewById(R.id.time_progressbar);
        volumeSeekBar = (SeekBar) findViewById(R.id.volume_seek_bar);
        volumeSeekBar.setMax(maxVolume);
        volumeSeekBar.setProgress(currentVolume);
        volumeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void setMedia() {
        if (audioManager == null) {
            audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        }
        if (mediaPlayer == null) {
            mediaPlayer = createMediaPlayer();
        }
        tickHandler = new TickHandler(mediaPlayer,
                (TextView) findViewById(R.id.sound_time_current), (TextView) findViewById(R.id.sound_time_max));
    }

    private int getCurrentVolume() {
        return audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
    }

    private MediaPlayer createMediaPlayer() {
        MediaPlayer player = MediaPlayer.create(this, soundResource);
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                tickHandler.setPlayFlag(false);
                releaseMediaPlayer();
                startActivity(FinishActivity.class);
                finish();
            }
        });
        return player;
    }

    private void soundAction() {
        if (mediaPlayer.isPlaying()) {
            pauseSound();
        }
        else {
            playSound();
        }
    }

    private void playSound() {
        if (!mediaPlayer.isPlaying()) {
            mediaPlayer.start();
            soundButton.setImageResource(R.mipmap.pause_btn);
            tickHandler.setPlayFlag(true);
            tickHandler.sleep(0);
        }
    }

    private void pauseSound() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            tickHandler.setPlayFlag(false);
            soundButton.setImageResource(R.mipmap.play_btn);
        }
    }

    private void stopSound() {
        if (mediaPlayer.isPlaying()) {
            tickHandler.setPlayFlag(false);
            mediaPlayer.stop();
        }
    }

    private void releaseMediaPlayer() {
        if (mediaPlayer == null) {
            return;
        }
        if (mediaPlayer.isPlaying()) {
            stopSound();
        }
        mediaPlayer.release();
        mediaPlayer = null;
    }

    private void updateSeekBarSoundState() {
        volumeSeekBar.setProgress(getCurrentVolume());
    }

    private void updateSoundProgress(int progress, int max) {
        soundProgressBar.setMax(max);
        soundProgressBar.setProgress(progress);
    }

    public class TickHandler extends Handler {
        private boolean playFlag;
        private MediaPlayer mp;
        private TextView currentTimeText;
        private TextView maxTimeText;
        private String durationTime;
        private String currentTime;

        public TickHandler(MediaPlayer mp, TextView current, TextView max) {
            super();
            this.mp = mp;
            currentTimeText = current;
            maxTimeText = max;
        }

        public void setPlayFlag(boolean flag) {
            playFlag = flag;
        }

        @Override
        public void handleMessage(Message msg) {
            if (playFlag) {
                int duration = mp.getDuration();
                int currentPosition = mp.getCurrentPosition();

                durationTime = timeText(duration);
                currentTime = timeText(currentPosition);
                updateSoundProgress(currentPosition, duration);
            }
            currentTimeText.setText(currentTime);
            maxTimeText.setText(durationTime);
            this.sleep(50);
        }

        public void sleep(long delayMills) {
            removeMessages(0);
            sendMessageDelayed(obtainMessage(0), delayMills);
        }

        private String timeText(int time) {
            int m = (time / 60000);
            int s = (time % 60000 / 1000);

            String str = "00:00";
            if (m > 10 && s > 10) str = "" + m + ":" + s;
            else if (m < 10 && s >= 10) str = "0" + m + ":" + s;
            else if (m >= 10 && s < 10) str = "" + m + ":" + "0" + s;
            else if (m < 10 && s < 10) str = "0" + m + ":" + "0" + s;

            return str;
        }
    }
}

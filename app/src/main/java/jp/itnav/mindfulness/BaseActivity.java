package jp.itnav.mindfulness;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener {
    protected static final String SHARED_DATA_NAME = "preferences";
    protected static final String SHARED_KEY_TUTORIAL_STATUS = "tutorial";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void startActivity(Class<?> activity) {
        Intent intent = new Intent(this, activity);
        startActivity(intent);
        overridePendingTransition(R.anim.in_activity, R.anim.out_activity);
    }

    protected void startActivity(Class<?> activity, String key, int param) {
        Intent intent = new Intent(this, activity);
        intent.putExtra(key, param);
        startActivity(intent);
        overridePendingTransition(R.anim.in_activity, R.anim.out_activity);
    }

    protected boolean isFinishedTutorial() {
        return getSharedPreferences().getBoolean(SHARED_KEY_TUTORIAL_STATUS, false);
    }

    protected void setTutorialFinishedStatus(boolean isFinished) {
        SharedPreferences.Editor editor = getSharedPreferencesEditor();
        editor.putBoolean(SHARED_KEY_TUTORIAL_STATUS, isFinished);
        editor.apply();
    }

    private SharedPreferences getSharedPreferences() {
        return getSharedPreferences(SHARED_DATA_NAME, Context.MODE_PRIVATE);
    }

    private SharedPreferences.Editor getSharedPreferencesEditor() {
        SharedPreferences preferences = getSharedPreferences(SHARED_DATA_NAME, Context.MODE_PRIVATE);
        return preferences.edit();
    }
}

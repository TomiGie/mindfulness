package jp.itnav.mindfulness;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import jp.itnav.mindfulness.Data.SoundsData;

public class SelectSoundAdapter extends RecyclerView.Adapter<SelectSoundAdapter.ViewHolder> {

    public interface OnRecyclerViewClickListener {
        void onRecyclerViewItemClick(View v, int position, int[] soundsId, int soundLogo);
    }

    private LayoutInflater layoutInflater;
    private SoundsData soundsData;
    private OnRecyclerViewClickListener onRecyclerViewClickListener;


    public SelectSoundAdapter(Context context, SoundsData data, OnRecyclerViewClickListener listener) {
        super();
        layoutInflater = LayoutInflater.from(context);
        soundsData = data;
        onRecyclerViewClickListener = listener;
    }

    @Override
    public SelectSoundAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.sound_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final SelectSoundAdapter.ViewHolder holder, final int position) {
        final SoundsData.SoundData data = soundsData.getSoundData(position);
        holder.soundLogo.setImageResource(data.getSoundLogoId());
        holder.soundTitle.setText(data.getSoundTitle());
        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecyclerViewClickListener.onRecyclerViewItemClick(v, position, data.getSoundsId(), data.getSoundLogoId());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (soundsData != null) {
            return soundsData.getSoundsDataSize();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout rootView;
        public ImageView soundLogo;
        public TextView soundTitle;

        @SuppressLint("NewApi")
        public ViewHolder(View v) {
            super(v);
            rootView = (LinearLayout) v.findViewById(R.id.root_sound_item);
            soundLogo = (ImageView) v.findViewById(R.id.sound_item_logo);
            soundTitle = (TextView) v.findViewById(R.id.text_sound_title);
        }
    }
}
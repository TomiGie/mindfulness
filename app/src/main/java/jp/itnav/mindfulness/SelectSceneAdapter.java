package jp.itnav.mindfulness;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import jp.itnav.mindfulness.Data.ScenesData;

public class SelectSceneAdapter extends RecyclerView.Adapter<SelectSceneAdapter.ViewHolder> {

    public interface OnRecyclerViewClickListener {
        void onRecyclerViewItemClick(View v, int position, ScenesData.SceneData data);
    }

    private ScenesData scenesData;
    private LayoutInflater layoutInflater;
    private OnRecyclerViewClickListener onRecyclerViewClickListener;


    public SelectSceneAdapter(Context context, ScenesData data, OnRecyclerViewClickListener listener) {
        super();
        layoutInflater = LayoutInflater.from(context);
        scenesData = data;
        onRecyclerViewClickListener = listener;
    }

    @Override
    public SelectSceneAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.scene_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final SelectSceneAdapter.ViewHolder holder, final int position) {
        final ScenesData.SceneData data = scenesData.getSceneData(position);
        holder.sceneImage.setImageResource(data.getSceneImageId());
        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecyclerViewClickListener.onRecyclerViewItemClick(v, position, data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return scenesData.getScenesDataSize();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout rootView;
        public ImageView sceneImage;

        @SuppressLint("NewApi")
        public ViewHolder(View v) {
            super(v);
            rootView = (LinearLayout) v.findViewById(R.id.root_sound_item);
            sceneImage = (ImageView) v.findViewById(R.id.scene_select_image);
        }
    }
}
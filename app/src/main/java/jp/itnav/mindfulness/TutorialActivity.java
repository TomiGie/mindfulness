package jp.itnav.mindfulness;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

public class TutorialActivity extends BaseActivity {

    private VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        setLayout();
        setVideo();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.image_tutorial_play:
                startVideo();
                v.setVisibility(View.GONE);
        }
    }

    private void setLayout() {
        ImageView playImage = (ImageView) findViewById(R.id.image_tutorial_play);
        playImage.setOnClickListener(this);
    }

    private void setVideo() {
        videoView = (VideoView) findViewById(R.id.view_tutorial);
        videoView.setVideoURI(Uri.parse("android.resource://" + this.getPackageName() + "/" + R.raw.tutorial));
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                tutorialFinished();
            }
        });
        MediaController controller = new MediaController(this) {
        };
        videoView.setMediaController(controller);
        controller.show();
    }

    private void tutorialFinished() {
        if (!isFinishedTutorial()) {
            setTutorialFinishedStatus(true);
            startActivity(SelectSceneActivity.class);
        }
        finish();
    }

    private void startVideo() {
        videoView.start();
    }
}

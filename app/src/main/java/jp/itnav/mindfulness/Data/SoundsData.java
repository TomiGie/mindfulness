package jp.itnav.mindfulness.Data;

import java.util.ArrayList;

import jp.itnav.mindfulness.R;


public class SoundsData {
    private ArrayList<SoundData> soundsList;

    private static final int[][] SOUND_LIST = {
            { R.raw.bird01min, R.raw.bird05min, R.raw.bird10min, R.raw.bird20min, R.raw.bird20min, R.raw.bird20min },
            { R.raw.bird01min, R.raw.bird05min, R.raw.bird10min, R.raw.bird20min, R.raw.bird20min, R.raw.bird20min },
            { R.raw.bird01min, R.raw.bird05min, R.raw.bird10min, R.raw.bird20min, R.raw.bird20min, R.raw.bird20min },
            { R.raw.bird01min, R.raw.bird05min, R.raw.bird10min, R.raw.bird20min, R.raw.bird20min, R.raw.bird20min },
            { R.raw.bird01min, R.raw.bird05min, R.raw.bird10min, R.raw.bird20min, R.raw.bird20min, R.raw.bird20min }
    };

    private final String[] soundTitleList = {
            "さざ波の音",
            "小鳥のさえずり",
            "流れる水の音",
            "森林浴の音",
            "夜の静かな音"
    };

    private final int[] soundLogoList = {
            R.mipmap.sea_logo,
            R.mipmap.morning,
            R.mipmap.creek_logo,
            R.mipmap.forest_logo,
            R.mipmap.night_logo
    };

    public SoundsData() {
        soundsList = createSoundsList();
    }

    public static int[] getDefaultSound() {
        return SOUND_LIST[0];
    }

    public SoundData getSoundData(int position) {
        return soundsList.get(position);
    }

    public int getSoundsDataSize() {
        return soundsList.size();
    }

    private ArrayList<SoundData> createSoundsList() {
        ArrayList<SoundData> dataList = new ArrayList<>();
        for (int i = 0; i < SOUND_LIST.length; i++) {
            dataList.add(new SoundData(SOUND_LIST[i], getSoundLogoId(i), getSoundTitle(i)));
        }
        return dataList;
    }

    ////
    // Out Of Bounds 対策
    ////
    private int getSoundLogoId(int index) {
        if (soundLogoList.length <= index) {
            return 0;
        }
        else {
            return soundLogoList[index];
        }
    }

    private String getSoundTitle(int index) {
        if (soundTitleList.length <= index) {
            return "";
        }
        else {
            return soundTitleList[index];
        }
    }
    // --- //

    public class SoundData {
        private final int[] soundsId;
        private final int soundLogoId;
        private final String soundTitle;

        public SoundData (int[] soundId, int resId, String title) {
            this.soundsId = soundId;
            soundLogoId = resId;
            soundTitle = title;
        }

        public int[] getSoundsId() {
            return soundsId;
        }

        public int getSoundId(int index) {
            return soundsId[index];
        }

        public int getSoundLogoId() {
            return soundLogoId;
        }

        public String getSoundTitle() {
            return soundTitle;
        }
    }
}

package jp.itnav.mindfulness.Data;

import java.util.ArrayList;

import jp.itnav.mindfulness.R;


public class ScenesData {
    private ArrayList<SceneData> sceneList;

    private String[] sceneTitleList = {
            "忙しい時",
            "リフレッシュしたい時",
            "集中したい時",
            "リラックスしたい時",
            "深く瞑想"
    };

    private String[] timeList = {
            "1",
            "10",
            "20",
            "30",
            "40"
    };

    // ダイアログ用イメージ
    private int[] sceneImageList = {
            R.mipmap.select_1,
            R.mipmap.select_10,
            R.mipmap.select_20,
            R.mipmap.select_30,
            R.mipmap.select_40
    };

    // SceneSelectActivity用イメージ
    private int[] sceneDisplayImageList = {
        R.mipmap.display_1,
        R.mipmap.display_10,
        R.mipmap.display_20,
        R.mipmap.display_30,
        R.mipmap.display_40
    };

    public ScenesData() {
        sceneList = createSceneList();
    }

    public SceneData getSceneData(int position) {
        return sceneList.get(position);
    }

    public int getScenesDataSize() {
        return sceneList.size();
    }

    public int getDisplayImage(int index) {
        if (index <= sceneDisplayImageList.length) {
            return sceneDisplayImageList[index];
        }
        else {
            return sceneDisplayImageList[0];
        }
    }

    private ArrayList<SceneData> createSceneList() {
        ArrayList<SceneData> dataList = new ArrayList<>();
        for (int i = 0; i < sceneImageList.length; i++) {
            dataList.add(new SceneData(getSceneTitle(i), getTimeLength(i), getSceneImage(i), getDisplayImage(i)));
        }
        return dataList;
    }

    ////
    // Out Of Bounds 対策
    ////
    private String getSceneTitle(int index) {
        if (sceneTitleList.length <= index) {
            return sceneTitleList[index];
        }
        else {
            return sceneTitleList[0];
        }
    }

    private String getTimeLength(int index) {
        if (timeList.length <= index) {
            return "";
        }
        else {
            return timeList[index];
        }
    }

    private int getSceneImage(int position) {
        if (position <= sceneImageList.length) {
            return sceneImageList[position];
        }
        else {
            return sceneImageList[0];
        }
    }
    // --- //

    public class SceneData {
        private final String sceneTitle;
        private final String sceneTime;
        private final int sceneImageId;
        private final int displayImageId;

        public SceneData (String title, String time, int resId, int displayResId) {
            sceneTitle = title;
            sceneTime = time;
            sceneImageId = resId;
            displayImageId = displayResId;
        }

        public String getSceneTitle() {
            return sceneTitle;
        }

        public String getSceneTime() {
            return sceneTime;
        }

        public int getSceneImageId() {
            return sceneImageId;
        }

        public int getDisplayImageId() {
            return displayImageId;
        }
    }
}

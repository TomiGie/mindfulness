package jp.itnav.mindfulness;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class FinishActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish);
        setLayout();
    }

    @Override
    public void onClick(View v) {
        finish();
    }

    private void setLayout() {
        ImageButton toTopBtn = (ImageButton) findViewById(R.id.button_to_top);
        toTopBtn.setOnClickListener(this);
    }
}

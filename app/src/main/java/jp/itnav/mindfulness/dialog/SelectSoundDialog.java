package jp.itnav.mindfulness.dialog;


import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import jp.itnav.mindfulness.Data.SoundsData;
import jp.itnav.mindfulness.R;
import jp.itnav.mindfulness.SelectSoundAdapter;


public abstract class SelectSoundDialog extends BaseCustomDialog implements View.OnClickListener{

    public abstract void onDismissSoundDialog(int[] selectedSoundsList, int soundImageResourceId);
    private int[] selectedSoundsList;
    private int selectedSoundImage = 0;
    private SoundsData soundsData;
    private MediaPlayer mediaPlayer;

    @Override
    public void onPause() {
        super.onPause();
        if (activityDialog.isShowing()) {
            activityDialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        releaseMediaPlayer();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Context context = getActivity().getApplicationContext();
        SelectSoundAdapter adapter = new SelectSoundAdapter(context, soundsData, new SelectSoundAdapter.OnRecyclerViewClickListener() {
            @Override
            public void onRecyclerViewItemClick(View v, int position, int[] soundsId, int soundLogoId) {
                selectedSoundsList = soundsId;
                selectedSoundImage = soundLogoId;
                releaseMediaPlayer();
                mediaPlayer = MediaPlayer.create(context, soundsId[0]);
                mediaPlayer.start();
            }
        });
        RecyclerView recyclerView = (RecyclerView) getDialog().findViewById(R.id.recycler_view_sound_list);
        recyclerView.setLayoutManager(new GridLayoutManager(context, 1));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        activityDialog.findViewById(R.id.button_sound_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    protected int getLayout() {
        return R.layout.dialog_select_sound;
    }

    @Override
    protected double getWidthRatio() {
        return 0.98;
    }

    @Override
    protected double getHeightRatio() {
        return 0.9;
    }

    @Override
    protected void setLayout(Dialog dialog) {
        super.setLayout(dialog);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        onDismissSoundDialog(selectedSoundsList, selectedSoundImage);
    }

    @Override
    public void onClick(View v) {
    }

    public void releaseMediaPlayer() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }
    }

    public void show(FragmentManager manager, String tag, SoundsData data) {
        if (manager != null) {
            soundsData = data;
            super.show(manager, tag);
        }
    }
}

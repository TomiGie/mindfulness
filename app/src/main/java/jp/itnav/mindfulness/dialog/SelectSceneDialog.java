package jp.itnav.mindfulness.dialog;


import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import jp.itnav.mindfulness.Data.ScenesData;
import jp.itnav.mindfulness.R;
import jp.itnav.mindfulness.SelectSceneAdapter;


public abstract class SelectSceneDialog extends BaseCustomDialog {
    public abstract void onDismissSceneDialog(ScenesData.SceneData selectedSceneData, int selectedPosition);
    private ScenesData scenesData;
    private ScenesData.SceneData selectedSceneData;
    private int selectedPosition;

    @Override
    public void onPause() {
        super.onPause();
        if (activityDialog.isShowing()) {
            activityDialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (selectedSceneData != null) {
            onDismissSceneDialog(selectedSceneData, selectedPosition);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Context context = getActivity().getApplicationContext();
        SelectSceneAdapter adapter = new SelectSceneAdapter(context, scenesData, new SelectSceneAdapter.OnRecyclerViewClickListener() {
            @Override
            public void onRecyclerViewItemClick(View v, int position, ScenesData.SceneData data) {
                selectedSceneData = data;
                selectedPosition = position;
                dismiss();
            }
        });
        RecyclerView recyclerView = (RecyclerView) getDialog().findViewById(R.id.recycler_view_scene_list);
        recyclerView.setLayoutManager(new GridLayoutManager(context, 1));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected int getLayout() {
        return R.layout.dialog_select_scene;
    }

    @Override
    protected double getWidthRatio() {
        return 0.98;
    }

    @Override
    protected double getHeightRatio() {
        return 0.9;
    }

    @Override
    protected void setLayout(Dialog dialog) {
        super.setLayout(dialog);
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    public void show(FragmentManager manager, String tag, ScenesData data) {
        if (manager != null) {
            super.show(manager, tag);
            scenesData = data;
        }
    }
}

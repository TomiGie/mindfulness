package jp.itnav.mindfulness.dialog;


import android.app.Dialog;
import android.app.FragmentManager;
import android.view.View;

import jp.itnav.mindfulness.R;


public class TopMenuDialog extends BaseCustomDialog implements View.OnClickListener{

    @Override
    public void onPause() {
        super.onPause();
        if (activityDialog.isShowing()) {
            activityDialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected int getLayout() {
        return R.layout.dialog_top_menu;
    }

    @Override
    protected double getWidthRatio() {
        return 0.9;
    }

    @Override
    protected double getHeightRatio() {
        return 0.7;
    }

    @Override
    protected void setLayout(Dialog dialog) {
        super.setLayout(dialog);
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    public void show(FragmentManager manager, String tag) {
        if (manager != null) {
            super.show(manager, tag);
        }
    }

    @Override
    public void onClick(View v) {
    }
}

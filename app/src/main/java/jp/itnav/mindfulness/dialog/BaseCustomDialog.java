package jp.itnav.mindfulness.dialog;


import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;

public abstract class BaseCustomDialog extends DialogFragment {
    protected Dialog activityDialog;

    protected abstract int getLayout();
    protected abstract double getWidthRatio();
    protected abstract double getHeightRatio();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        activityDialog = new Dialog(getActivity());
        setLayout(activityDialog);
        return activityDialog;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Dialog dialog = getDialog();
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int dialogWidth = (int) (metrics.widthPixels * getWidthRatio());
        int dialogHeight = (int) (metrics.heightPixels * getHeightRatio());

        lp.width = dialogWidth;
        lp.height = dialogHeight;
        dialog.getWindow().setAttributes(lp);
    }

    protected void setLayout(Dialog dialog) {
        dialog.getWindow().requestFeature(getRequestFeature());
        dialog.setContentView(getLayout());
        dialog.getWindow().setBackgroundDrawable(getBackGroundDrawable());
    }

    protected ColorDrawable getBackGroundDrawable() {
        return new ColorDrawable(Color.TRANSPARENT);
    }

    protected int getRequestFeature() {
        return Window.FEATURE_NO_TITLE;
    }
}
